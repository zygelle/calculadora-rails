class CalculatorController < ApplicationController
    def home
        @first_number = params[:first_number].to_i
        @second_number = params[:second_number].to_i
    end

    def sum_result
        @result = params[:first_number].to_i + params[:second_number].to_i
    end

    def subtract_result
        @result = params[:first_number].to_i - params[:second_number].to_i
    end

    def multiply_result
        @result = params[:first_number].to_i * params[:second_number].to_i
    end

    def divide_result
        @result = params[:first_number].to_f / params[:second_number].to_f
    end
end