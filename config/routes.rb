Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #get '/calculadora/:name', to: 'calculator#home', as: 'home'
  get '/calculadora/:first_number/:second_number', to: 'calculator#home', as: 'home'
  get '/calculadora/:first_number/:second_number/soma', to: 'calculator#sum_result', as: 'sum_numbers'
  get '/calculadora/:first_number/:second_number/subtracao', to: 'calculator#subtract_result', as: 'subtract_numbers'
  get '/calculadora/:first_number/:second_number/multiplicacao', to: 'calculator#multiply_result', as: 'multiply_numbers'
  get '/calculadora/:first_number/:second_number/divisao', to: 'calculator#divide_result', as: 'divide_numbers'
end
